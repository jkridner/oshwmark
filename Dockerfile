FROM ubuntu:impish AS oshwmark-env
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt upgrade -y
RUN apt install -y vim curl wget

RUN curl --output /usr/share/keyrings/nginx-keyring.gpg  \
      https://unit.nginx.org/keys/nginx-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/nginx-keyring.gpg] https://packages.nginx.org/unit/ubuntu/ impish unit" > /etc/apt/sources.list.d/unit.list
RUN echo "deb-src [signed-by=/usr/share/keyrings/nginx-keyring.gpg] https://packages.nginx.org/unit/ubuntu/ impish unit" >> /etc/apt/sources.list.d/unit.list

RUN apt update && apt upgrade -y
RUN apt install -y ruby-full rails nginx unit unit-ruby

RUN apt install -y build-essential make

WORKDIR /opt/imagemagic
RUN echo . \
	&& wget https://download.imagemagick.org/ImageMagick/download/ImageMagick-7.1.0-37.tar.xz \
	&& tar -axvf ImageMagick-7.1.0-37.tar.xz \
	&& cd ImageMagick-7.1.0-37/ \
	&& ./configure \
	&& make install \
	&& ldconfig /usr/local/lib \
	&& cd .. \
	&& rm -rf ImageMagick-7.1.0-37/ ImageMagick-7.1.0-37.tar.xz \
	&& echo .

WORKDIR /opt/oshwmark
COPY . /opt/oshwmark/

